// Import the react and reactDOM libraries
import React from 'react';
import ReactDOM from 'react-dom';
import SeasonDisplay from './SeasonDisplay';
import Spinner from './Spinner';
import UserForm from './UserForm';
import Clock from './Clock';

// Create a react component using functional
// const App = () => {
  // window.navigator.geolocation.getCurrentPosition(
  //   (position) => console.log(position),
  //   (error) => console.log(error)
  // );

//   return (
//     <div className="ui container">
//       Latitude:
//     </div>
//   );
// }

// Create a react component using class
class App extends React.Component {
  // constructor(props) {
  //   super(props);

  //   // THIS IS THE ONLY TIME, we do direct assignment
  //   // to this.state
  //   this.state = { lat: null, errorMessage: '' };
  // }
  state = { lat: null, errorMessage: '' };

  componentDidMount() {
    window.navigator.geolocation.getCurrentPosition(
      (position) => {
        // We called setState!!!!
        this.setState({ lat: position.coords.latitude })

        // we did not !!
        // this.state.lat = position.coords.latitude;
      },
      (error) => {
        this.setState({ errorMessage: error.message });
      }
    );
  }


  componentDidUpdate() {

  }

  renderContent() {
    if (this.state.errorMessage && !this.state.lat) {
      return (
        <div className="border red">
          Error: {this.state.errorMessage}
        </div>
      );
    }
    if (!this.state.errorMessage && this.state.lat) {
      return <SeasonDisplay lat={this.state.lat} />;
    }

    return <Spinner message="Please accept location request" />;
  }


  // React says we have to define render!!
  render() {
    return (
      <div className="border red">
        {this.renderContent()}
        <UserForm />
        <Clock />
      </div>
    )

  }
}

// Take the react component and show it on the screen
ReactDOM.render( <App />, document.querySelector('#root'));
