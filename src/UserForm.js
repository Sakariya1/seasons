// Import the react and reactDOM libraries
import React from 'react';

// Create a react component using class
class UserForm extends React.Component {
  state = { labelName: 'Enter a username' };
  // React says we have to define render!!
  render() {
    return (
      <div className="border red">
        <form>
          <label>{this.state.labelName}</label>
          <input type="text" />
        </form>
      </div>
    );
  };
}

export default UserForm;
