// Import the react and reactDOM libraries
import React from 'react';

// Create a react component using class
class Clock extends React.Component {
  state = { time: new Date().toLocaleTimeString() };

  componentDidMount() {
    setInterval(() => {
      this.setState({ time: new Date().toLocaleTimeString() });
    }, 1000);
  }


  // React says we have to define render!!
  render() {
    return (
      <div className="border red">
        The time is: {this.state.time}
      </div>
    );
  };
}

export default Clock;
